/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import javax.swing.JOptionPane;

/**
 *
 * @author DANIELA ALEJANDRA MAMANI SILVA
 */
public class ejer10 {
    public static void main (String[]args){
        String input=JOptionPane.showInputDialog("introduzca un numero");
        int num=Integer.parseInt(input);
        if(tresdig(num)){
            String result =cifras(num);
            JOptionPane.showMessageDialog(null, result);
        }
        else {
            JOptionPane.showMessageDialog(null,"error solo introduzca un numero de 3 digitos");
        }
    }
    public static String cifras(int a){
        int b,c,d; 
        b=a/100;
        c=(a/10)%10;
        d=(a%10);
        return "las cifras que tiene el numero son: "+b+" - "+c+" - "+d;
    }
    public static boolean tresdig(int a){
        int count =0;
        for (int i=a;i>0;i=i/10){
            count++;
        }
        return count==3;
    }
}
