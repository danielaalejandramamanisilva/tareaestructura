/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import javax.swing.JOptionPane;

/**
 *
 * @author DANIELA ALEJANDRA MAMANI SILVA
 */
public class ejer7 {
    public static void main(String[]args){
        int number;
        String input = JOptionPane.showInputDialog("ingrese un numero");
        number=dig(Integer.parseInt(input));
        JOptionPane.showMessageDialog(null,"el numero de cifras que tiene el numero: "+input+" es: "+ number);
    }
   public static int dig(int a){
       int count=0;
        for(int i=a;i>0;i/=10){
            count++;
        }
        return count;
   }
}
