/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;
import javax.swing.JOptionPane;
/**
 *
 * @author DANIELA ALEJANDRA MAMANI SILVA
 */
public class ejer6 {
    public static void main(String[]args){
        String input="";
        input=JOptionPane.showInputDialog("Inserte un numero");
        int dec=Integer.parseInt(input);
        JOptionPane.showMessageDialog(null,"El binario de "+dec+" es: "+binary(dec));
    }
    public static String binary(int a){
        String binario="";
       if (a>0){
           while(a>0){
               int modulo=a%2;
               binario=modulo+binario;
               a=a/2;
           }
       }
       return binario;
    }
}
