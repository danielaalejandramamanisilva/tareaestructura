/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import javax.swing.JOptionPane;

/**
 *
 * @author DANIELA ALEJANDRA MAMANI SILVA
 */
public class ejer9 {
    public static void main(String[]args){
        String input;
        int a,b,c;
        input=JOptionPane.showInputDialog("introduzca el primer lado del triangulo");
        a=Integer.parseInt(input);
        input=JOptionPane.showInputDialog("introduzca el segundo lado del triangulo");
        b=Integer.parseInt(input);
        input=JOptionPane.showInputDialog("introduzca el tercer lado del triangulo");
        c=Integer.parseInt(input);
        JOptionPane.showMessageDialog(null,"el area del triangulo es: "+areatriangulo(a,b,c));
    }
    public static Double areatriangulo(double a, double b, double c){
        Double per=(a+b+c)/2;
        Double area=Math.sqrt(per*(per-a)*(per-b)*(per-c));
        return area;
    }
}

