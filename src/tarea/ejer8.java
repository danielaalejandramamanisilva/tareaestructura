/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author DANIELA ALEJANDRA MAMANI SILVA
 */
public class ejer8 {
    public static void main(String[]args){
        String inputtext=JOptionPane.showInputDialog("INTRODUZCA UNA CANTIDAD EN BOLIVIANOS");
        double bolivianos=Double.parseDouble(inputtext);
        String money = JOptionPane.showInputDialog("A QUE MONEDA DESEA CONVERTIR");
        JOptionPane.showMessageDialog(null, conversor (bolivianos, money));
    }
    public static String conversor(double cant, String tipodecambio){
        double resp=0;
        switch (tipodecambio){
            case "dolares":
                resp=cant*0.14451;
            break;
            case"euros":
                resp=cant*0.12938;
            break;
            case "libras":
                resp=cant*0.11552;
                break;
            default:
                JOptionPane.showMessageDialog(null, "EL TIPO DE MONEDA INGRESADO NO EXISTE");
                break;
        }
        DecimalFormat df=new DecimalFormat("#.00");
        return "LA CANTIDAD EN: "+tipodecambio+" ES: "+df.format(resp);
    }
}
